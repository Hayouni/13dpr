﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data.SqlClient;

namespace _13dpr
{
    public partial class Server : Form
    {

        int i;
       
        TcpListener server = new TcpListener(IPAddress.Any, 1980); // Creates a TCP Listener To Listen to Any IPAddress trying to connect to the program with port 1980
        NetworkStream stream; //Creats a NetworkStream (used for sending and receiving data)
        TcpClient client; // Creates a TCP Client
        byte[] datalength = new byte[4]; // creates a new byte with length 4 ( used for receivng data's lenght)
    
        public Server()
        {
            InitializeComponent();

            this.FormClosing += new FormClosingEventHandler(Server_FormClosing);
          // rapportDataGridView.Rows[1].DefaultCellStyle.BackColor = Color.FromArgb(250, 250, 0);
            //this.rapportTableAdapter.Fill(this.rapport._Rapport);
           //  ColorCase();

           // ServerSend(GetLocalIPAddress());
        }
      

        private void Server_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rapport._Rapport' table. You can move, or remove it, as needed.
            this.rapportTableAdapter.Fill(this.rapport._Rapport);
            // TODO: This line of code loads data into the 'receivedData.Received' table. You can move, or remove it, as needed.
         //   this.receivedTableAdapter.Fill(this.receivedData.Received);
            // TODO: This line of code loads data into the 'receivedData.Received' table. You can move, or remove it, as needed.
          //  this.receivedTableAdapter1.Fill(this.ReceivedData.Received);
            // TODO: This line of code loads data into the '_13DPRDataSet.Received' table. You can move, or remove it, as needed.
          //  this.receivedTableAdapter.Fill(this._13DPRDataSet.Received);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (client.Connected) // if the client is connected
            {
                ServerSend(txtSend.Text); // uses the Function ClientSend and the msg as txtSend.Text
            }
        }

        public void ServerReceive()
        {
            stream = client.GetStream(); //Gets The Stream of The Connection
            new Thread(() => // Thread (like Timer)
            {
                if (stream.DataAvailable)
                {

                    while ((i = stream.Read(datalength, 0, 4)) != 0)//Keeps Trying to Receive the Size of the Message or Data
                    {
                        // how to make a byte E.X byte[] examlpe = new byte[the size of the byte here] , i used BitConverter.ToInt32(datalength,0) cuz i received the length of the data in byte called datalength :D
                        byte[] data = new byte[BitConverter.ToInt32(datalength, 0)]; // Creates a Byte for the data to be Received On
                        stream.Read(data, 0, data.Length); //Receives The Real Data not the Size
                        this.Invoke((MethodInvoker)delegate // To Write the Received data
                        {
                            txtLog.Text += System.Environment.NewLine + "Client : " + Encoding.Default.GetString(data); // Encoding.Default.GetString(data); Converts Bytes Received to String
                            string msg = Encoding.Default.GetString(data);

                            /////////////////////////////////////////////////
                            ///////////////Split the worlds from msg to store them///////////////
                            /////////////////////////////////////////////////////
                            char[] delimiterChars = { '~' };
                            string[] words = msg.Split(delimiterChars);
                            string msg1 = words[0];
                            string msg2 = words[1];
                            string msg3 = words[2];
                            if (msg1 == "fix")
                            {
                                SqlConnection con = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=_13DPR;Integrated Security=True");

                                con.Open();
                                SqlCommand updatecom = new SqlCommand("UPDATE Rapport SET [Etat]='" + msg2 + "' WHERE [Reference]='" + msg3 + "' ", con);
                                {
                                    // updatecom.Parameters.AddWithValue("@rest", rest);
                                    try
                                    {

                                        updatecom.ExecuteNonQuery();
                                        con.Close();

                                        this.rapportTableAdapter.Fill(this.rapport._Rapport);
                                        ServerSend("Etat mise a jour");
                                    }
                                    catch (SqlException ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }
                                    ColorCase();
                                }


                            }
                            else
                            {

                                string msg4 = words[3];
                                string msg5 = words[4];

                                MessageBox.Show("Notification Concernant " + msg1 + ":" + msg3);
                                //////////////////////////////////////////////////////////
                                ////////////////Update the data base///////////

                                /////////////get the date/////////////
                                DateTime theDate;
                                theDate = DateTime.Now;
                                string DateString = theDate.ToString("yyyy-MM-dd");
                                /////////////////////////////////////
                                SqlConnection con = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=_13DPR;Integrated Security=True");

                                con.Open();
                                try
                                {
                                    SqlCommand cmd = new SqlCommand(@"INSERT INTO [_13DPR].[dbo].[Rapport]
(             
              [Navire] 
              , [Service] 
              , [Description] 
               ,[Date]
               ,[Etat]
,[Reference]
)
VALUES 
               ('" + msg1 + "','" + msg2 + "','" + msg3 + "','" + DateString + "','" + msg4 + "','" + msg5 + "')", con);
                                    cmd.ExecuteNonQuery();
                                    ColorCase();
                                    this.rapportTableAdapter.Fill(this.rapport._Rapport);
                                    ServerSend("Recue");
                                    con.Close();
                                    // this.receivedTableAdapter.Fill(this.receivedData.Received);
                                    //  this.receivedTableAdapter.Fill(this._13DPRDataSet.Received);
                                    // MessageBox.Show("Operation Reussie");
                                }
                                catch (Exception ex)
                                {
                                    ServerSend(ex.Message);
                                }
                                //////////////////////////////////////////
                            }
                        });
                    }
                }
            }).Start(); // Start the Thread
            
////////////////////////////////////////////////////////////////////////
          
        }
        public void ServerSend(string msg)
        {
            stream = client.GetStream(); //Gets The Stream of The Connection
            byte[] data; // creates a new byte without mentioning the size of it cuz its a byte used for sending
            data = Encoding.Default.GetBytes(msg); // put the msg in the byte ( it automaticly uses the size of the msg )
            int length = data.Length; // Gets the length of the byte data
            byte[] datalength = new byte[4]; // Creates a new byte with length of 4
            datalength = BitConverter.GetBytes(length); //put the length in a byte to send it
            stream.Write(datalength, 0, 4); // sends the data's length
            stream.Write(data, 0, data.Length); //Sends the real data
        }
 

        private void button1_Click(object sender, EventArgs e)
        {
            server.Start(); // Starts Listening to Any IPAddress trying to connect to the program with port 1980
            MessageBox.Show("Waiting For Connection");
            new Thread(() => // Creates a New Thread (like a timer)
            {
                client = server.AcceptTcpClient(); //Waits for the Client To Connect
                MessageBox.Show("Connected To Client");
                if (client.Connected) // If you are connected
                {
                    ServerReceive(); //Start Receiving
                }
            }).Start();
            ColorCase();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            stream.Close();
      
            server.Stop();
           
        }

        private void receivedBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.receivedBindingSource.EndEdit();
           // this.tableAdapterManager.UpdateAll(this._13DPRDataSet);
         //   this.receivedTableAdapter.Fill(this.receivedData.Received);
        }

        private void receivedDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        void ColorCase ()
        {
            int j = rapportDataGridView.RowCount-2 ;

            for (i = 0; i <= j; i++)
            {
                string txt = rapportDataGridView.Rows[i].Cells[4].Value.ToString();
               
                if (txt=="EN PANNE")
                {
                    rapportDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    
                }
                
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ColorCase();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            //button1.Show();
            SqlConnection con = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=_13DPR;Integrated Security=True");

            con.Open();
            //SqlDataAdapter a = new SqlDataAdapter("Select * From Users  ", con);
            using (SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM Rapport WHERE Navire = '"+comboBox1.Text+"'", con))
            {
                DataTable dt = new DataTable();


                sda.Fill(dt);
               rapportDataGridView.DataSource = dt;
                if (dt.Rows.Count == 0)
                {
                    //button1.Hide();
                    // label1.Hide();
                    MessageBox.Show("Aucune resultat pour votre recherche", "Desole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }


            }
            con.Close();
            ColorCase();
        }

        private void Server_FormClosing(object sender, FormClosingEventArgs e)
        {
            stream.Dispose();
            stream.Close();
            client.Close();
            server.Stop();
            Application.Exit(e);
            Environment.Exit(0);
        }
 
    }
}
