﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace _13dpr
{
    public partial class Client : Form
    {
       // string ip="";
        int i;
        TcpClient client; // Creates a TCP Client
        NetworkStream stream; //Creats a NetworkStream (used for sending and receiving data)
        byte[] datalength = new byte[4]; // creates a new byte with length 4 ( used for receivng data's lenght)
     
        public Client()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Client_FormClosing);
             //ip = ClientReceive();
   
            
        }
      
        private void Client_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            string ip = "192.168.1.102";
            try
            {
                client = new TcpClient(ip, 1980); //Trys to Connect
                ClientReceive(); //Starts Receiving When Connected
                MessageBox.Show("Connected To server");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); // Error handler :D
            }
        }
        public void ClientReceive()
        {
           // string result = "";
            stream = client.GetStream(); //Gets The Stream of The Connection
            new Thread(() => // Thread (like Timer)
            {
                while ((i = stream.Read(datalength, 0, 4)) != 0)//Keeps Trying to Receive the Size of the Message or Data
                {
                    // how to make a byte E.X byte[] examlpe = new byte[the size of the byte here] , i used BitConverter.ToInt32(datalength,0) cuz i received the length of the data in byte called datalength :D
                    byte[] data = new byte[BitConverter.ToInt32(datalength, 0)]; // Creates a Byte for the data to be Received On
                    stream.Read(data, 0, data.Length); //Receives The Real Data not the Size
                    ////////////////////////////////
                    if (Encoding.Default.GetString(data) == "Close")
                    {
                        stream.Close();
                        client.Close();
                        this.Close();
                    }
                    ////////////////////////////////////
                    this.Invoke((MethodInvoker)delegate // To Write the Received data
                    {
                        txtLog.Text =  "Server : " + Encoding.Default.GetString(data); // Encoding.Default.GetString(data); Converts Bytes Received to String
                        //result=  Encoding.Default.GetString(data); 
                    });
                }
            }).Start(); // Start the Thread
           // return result;
        }
        public void ClientSend(string msg1)
        {
            stream = client.GetStream(); //Gets The Stream of The Connection
            byte[] data1; // creates a new byte without mentioning the size of it cuz its a byte used for sending
            data1 = Encoding.Default.GetBytes(msg1); // put the msg in the byte ( it automaticly uses the size of the msg )
            int length1 = data1.Length; // Gets the length of the byte data
            byte[] datalength1 = new byte[4]; // Creates a new byte with length of 4
            datalength1 = BitConverter.GetBytes(length1); //put the length in a byte to send it
            stream.Write(datalength1, 0, 4); // sends the data's length
            stream.Write(data1, 0, data1.Length); //Sends the real data
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            String msg = comboBox1.Text + "~" + comboBox2.Text + "~" + textBox1.Text + "~" + comboBox3.Text + "~" + textBox3.Text;

            if (client.Connected) // if the client is connected
            {
                ClientSend(msg); // uses the Function ClientSend and the msg as txtSend.Text
            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        private void comboBox2_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            String msg = "fix" + "~" + textBox4.Text + "~" + textBox5.Text ;

            if (client.Connected) // if the client is connected
            {
                ClientSend(msg); // uses the Function ClientSend and the msg as txtSend.Text
            }
        }

        private void Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            Application.Exit(e);
            Environment.Exit(0);
        }
    }
}
